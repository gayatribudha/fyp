import os
import sys
import json
import flask

# Flask
from flask import Flask, redirect, url_for, request, render_template, Response, jsonify, redirect
from werkzeug.utils import secure_filename
from gevent.pywsgi import WSGIServer

# TensorFlow and tf.keras
import tensorflow as tf
from tensorflow import keras

from tensorflow.keras.preprocessing.image import ImageDataGenerator, img_to_array, load_img, array_to_img
from tensorflow.keras.applications.imagenet_utils import preprocess_input, decode_predictions
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing import image

# Some utilites
import numpy as np
from PIL import Image
from util import base64_to_pil

import matplotlib.pyplot as plt



# Declare a flask app
app = Flask(__name__)


# You can use pretrained model from Keras
# Check https://keras.io/applications/
# from keras.applications.mobilenet_v2 import MobileNetV2
# model = MobileNetV2(weights='imagenet')

# print('Model loaded. Check http://127.0.0.1:5000/')




# def model_predict(img, model):

#     # img = image.load_img(img_path, target_size=(300, 300))
  

#     img = img.resize((300, 300))
#     img = image.img_to_array(img, dtype=np.uint8)
#     img=np.array(img)/255.0

#     # # Preprocessing the image
#     # x = image.img_to_array(img, stype=np.uint8)
#     # # x = np.true_divide(x, 255)
#     # x = np.array(x)/255.0

#     # Be careful how your trained model deals with the input
#     # otherwise, it won't make correct prediction!
#     # x = preprocess_input(x, mode='tf')

#     # preds=model.predict(x[np.newaxis, ...])
#     # preds = model.predict(x)
#     return preds



@app.route('/', methods=['GET'])
def index():
    # Main page
    return render_template('index.html')


@app.route('/predict', methods=['GET', 'POST'])
def predict():
    if request.method == 'POST':
        # Get the image from post request
       
          
        # img = base64_to_pil(request.json)
       
        img = flask.request.files.get('image','')
       
        print("helllllllllllllllllllllllllllllllllllllllllllllllll")

        # Save the image to ./uploads
        # img.save("./uploads/image.png")



        # Model saved with Keras model.save()
        MODEL_PATH = 'trained_model(1).h5'

        # Load your own trained model
        global model
        model = load_model(MODEL_PATH)
        # model._make_predict_function()          # Necessary
        print('Model loaded. Start serving...')


        #Test Images Augmentation
        test=ImageDataGenerator(rescale=1/255,
                                validation_split=0.1)


        #Test set preparation for testing
        # dir_path = '/content/drive/My Drive/Garbage classification'
        dir_path = 'Garbage classification'
        test_generator=test.flow_from_directory(dir_path,
                                                target_size=(300,300),
                                                batch_size=32,
                                                class_mode='categorical',
                                                subset='validation')



        #Labelling the train set
        global labels
        labels = (test_generator.class_indices)
        print(labels)

        #Labelling the test set
        labels = dict((v,k) for k,v in labels.items())
        print(labels)

    
        img = img.resize((300, 300))
        img = image.img_to_array(img, dtype=np.uint8)
        img=np.array(img)/255.0

        # Make prediction
        # p = model_predict(img, model)

        p = model.predict(img[np.newaxis, ...])

        max_probability = np.max(p[0], axis=-1)
        print("Maximum Probability: ",np.max(p[0], axis=-1))
        predicted_class = labels[np.argmax(p[0], axis=-1)]
        print("Classified:",predicted_class)

        # # # Process your result for human
        # pred_proba = "{:.3f}".format(np.amax(p))     # Max probability
        # pred_class = decode_predictions(p)   # ImageNet Decode

        # result = str(pred_class[0][0][1])               # Convert to string
        # result = result.replace('_', ' ').capitalize()

        # pred_proba = max_probability
    
        # pred_class = predicted_class

        # result = str(pred_class)               # Convert to string
        # result = result.capitalize()

        
        # Serialize the result, you can add additional fields
                #print("Predicted shape",p.shape)
        




        # return jsonify(result=result, probability=pred_proba)
        return render_template('index.html', max_probability=max_probability, predicted_class=predicted_class)

    return None


if __name__ == '__main__':
    app.run(port=5002, threaded=False)

    # Serve the app with gevent
    # http_server = WSGIServer(('0.0.0.0', 5000), app)
    # http_server.serve_forever()
